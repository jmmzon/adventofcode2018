const {loadInputFile, increment, findMax, get, findMaxBy} = require('../libs');

const timestampRegex = /\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})]/;
const actionRegex = /(wakes up|falls asleep|begins shift)/;
const guardIDRegex = /#(\d*)/;

function* dateMinuteIterator(startDate, endDate) {
  const _endDate = new Date(endDate);
  let currentDate = new Date(startDate);

  while (currentDate < _endDate) {
    yield currentDate;
    currentDate = new Date(currentDate.getTime() + 60000);
  }
}

/**
 * @typedef {object} Interval
 * @property {Date|string} start
 * @property {Date|string} end
 */

/**
 * @property {number} Minute 0-60
 */

/**
 * @typedef {object} Guard
 * @property id
 * @property {Object.<Minute, number>} minutes
 * @property {Interval[]} intervals
 * @property {number} minutesSum
 */


/**
 *
 * @param record
 * @returns {Date}
 */
export const getDateFromRecord = record => new Date(timestampRegex.exec(record)[1]);
export const getGuardID = record => guardIDRegex.exec(record)[1];

export const getGuardsSleepIntervals = records => {
  let currentGuard = null;
  let fallenAsleepAt = null;
  return records.reduce((guards, record) => {
    switch (actionRegex.exec(record)[1]) {
      case 'begins shift': {
        currentGuard = getGuardID(record);
        break;
      }

      case 'falls asleep': {
        fallenAsleepAt = getDateFromRecord(record);
        break;
      }

      case 'wakes up': {
        const wakeup = getDateFromRecord(record);
        guards[currentGuard] = guards[currentGuard] || {intervals: [], id: currentGuard};
        guards[currentGuard].intervals.push({start: fallenAsleepAt, end: wakeup});
        break;
      }

    }
    return guards;
  }, {});
};

/**
 *
 * @param {Guard[]} guards
 * @returns {*}
 */
export const getMinutesInfo = guards =>
  guards.map(guard => {
    guard.minutes = {};
    guard.minutesSum = 0;
    guard.intervals.forEach(({start, end}) => {
      for (const time of dateMinuteIterator(start, end)) {
        guard.minutes[time.getMinutes()] = increment(guard.minutes[time.getMinutes()]);
        guard.minutesSum++;
      }
    });
    return guard;
  });

/**
 *
 * @param {Guard[]} guards
 * @returns {Guard}
 */
const findTheMostLazyGuy = (guards) => findMaxBy(guards, 'minutesSum');
/**
 *
 * @param {Guard[]} guards
 * @returns {*}
 */
const findMostlyUsedMinute = guards => {
  const guardFavouriteMinute = guard => {
    const [minute, days] = findMax(get(guard, 'minutes'));
    return {
      minute,
      days,
    };
  };

  const guardsFavouritesMinutes = guards.map(guard => {
    return {
      id: guard.id,
      ...guardFavouriteMinute(guard),
    };
  });

  return findMaxBy(guardsFavouritesMinutes, 'days');
};



export default function main([filename = 'input.txt']) {
  const fileContent = loadInputFile(filename);
  const sortedFileContent = fileContent.sort((a, b) => getDateFromRecord(a) - getDateFromRecord(b));

  const guardsSleepIntervals = Object.values(getGuardsSleepIntervals(sortedFileContent));
  const guardsMinutesSummarize = getMinutesInfo(guardsSleepIntervals);

  const theMostLazyGuy = findTheMostLazyGuy(guardsMinutesSummarize);
  const [theMoustLazyGuyMinutes] = findMax(theMostLazyGuy.minutes);
  const partTwoGuard = findMostlyUsedMinute(guardsMinutesSummarize);

  console.log('Part I result', +theMostLazyGuy.id * theMoustLazyGuyMinutes);
  console.log('Part II result', partTwoGuard.id * partTwoGuard.minute);

}


