import path from 'path';

const index = ([command, script, day, input = `${path.resolve(__dirname)}/${day}/input.txt`, ...args]) => {
    if(!day) {
        console.error('missing "day" param:');
        console.info(`run this program like ${command} ${script} day [inputFile]`);
        process.exit(1);
    }
    const subprogram = require(`./${day}`);
    subprogram.default([input, ...args]);
};

index(process.argv);
