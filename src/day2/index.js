import {
  loadInputFile,
  increment,
  has,
  sum,
  keyBy,
  positionalIntersection,
  positionalDifference, wordToChars,
} from '../libs';


export const countCharacters = word =>
  word.split('').reduce((acc, char) => {
    acc[char] = increment(acc[char]);
    return acc;
  }, {});

/**
 * Returns first occurence of string that differs at one position
 * @param first
 * @param rest
 * @returns {*}
 */
export const findDifferenceAtOne = ([first, ...rest]) => {
  if(!rest.length) {
    return [];
  }
  const second = (rest.find(w => positionalDifference(w.split(''), first.split('')).length === 1));
  if (second) {
    return [first, second];
  }
  return findDifferenceAtOne(rest);
};

export default function main([filename = 'input.txt']) {
  const fileContent = loadInputFile(filename);

  const partOneResult = fileContent.reduce((acc, word) => {
    const charsCount = keyBy(Object.values(countCharacters(word)));
    acc[2] = sum(acc[2], has(charsCount, 2));
    acc[3] = sum(acc[3], has(charsCount, 3));
    return acc;
  }, {});


  console.log('Part one result: ', partOneResult[2] * partOneResult[3]);
  console.log('Part two result: ', positionalIntersection(
      ...findDifferenceAtOne(fileContent).map(wordToChars)).join('')
  );
}


