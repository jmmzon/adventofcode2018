import {
    findMaxBy,
    findMinBy,
    loadInputFile,
    unary,
    range,
    get,
    partial,
    sum,
    first, isEqual, increment, last, isUndefined, pipe,
} from '../libs';

const MAX_TOTAL_DISTANCE = 10000;
const X = 0;
const Y = 0;

export const distance = ([x1, y1], [x2, y2]) =>
    Math.abs(x1 - x2) + Math.abs(y1 - y2);

export const getDistances = (nodes, p) =>
    nodes.map(unary(partial(distance, p)));

export const closestIndex = (distances) => {
    const isMinDistance = partial(isEqual, Math.min(...distances));
    const minDistances = distances.filter(isMinDistance);
    return minDistances.length === 1 ? distances.findIndex(isMinDistance) : -1;
};

export const matrixLoop = (topLeft, bottomRight, fn) => {
    for (const x of range(topLeft[X], bottomRight[X])) {
        for (const y of range(topLeft[Y], bottomRight[Y])) {
            fn(x, y);
        }
    }
};

export default function main([filename = 'input.txt']) {
    const input = loadInputFile(filename);
    const parsedInput = input.map(v => v.split(',').map(unary(parseInt)));

    const [minX] = findMinBy(parsedInput, X);
    const [, minY] = findMinBy(parsedInput, Y);
    const [maxX] = findMaxBy(parsedInput, X);
    const [, maxY] = findMaxBy(parsedInput, Y);
    const infinityNodes = {};
    const nodesUsages = new Map();
    let regions = 0;

    matrixLoop([minX, minY], [maxX, maxY], (x, y) => {
        const distances = getDistances(parsedInput, [x, y]);
        const index = closestIndex(distances);

        if (distances.reduce(sum) < MAX_TOTAL_DISTANCE) {
            regions++;
        }

        if (x === minX || x === maxX || y === minY || y === maxY) {
            infinityNodes[index] = true;
        } else {
            if (index >= 0) {
                nodesUsages.set(index, increment(nodesUsages.get(index)));
            }
        }
    });

    const partOne = [...nodesUsages].filter(pipe(
        unary(first),
        partial(get, infinityNodes),
        isUndefined
    ));

    console.log('Part one result:', last(findMaxBy(partOne, 1)));
    console.log('Part two result:', regions);
};
