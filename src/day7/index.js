import {groupByReducer, loadInputFile, prop, words, flatten, partialRight, first, unary} from '../libs';

import {WORKERS_LIMIT, BASE_EXECUTIOM_TIME} from './constants';

export const taskTime = taskId => taskId.charCodeAt(0) - 'A'.charCodeAt(0) + BASE_EXECUTIOM_TIME + 1;

export function buildDependenciesInfo(inputData) {
    return inputData
        .map(words)
        .reduce(groupByReducer(prop(1), prop(7)), {});
}

export function findLastTask(successorsGroupedByPredecessors) {
    return first(flatten(successorsGroupedByPredecessors).filter(function hasParsedhasPostJob(postJob) {
        return !Object.keys(successorsGroupedByPredecessors).includes(postJob);
    }));
}

export function simpleScheduler(inputData) {
    const successorsGroupedByPredecessors = buildDependenciesInfo(inputData);
    const lastTask = findLastTask(successorsGroupedByPredecessors);
    let queue = [];

    while (Object.values(successorsGroupedByPredecessors).length) {
        const predecessorTasks = Object.keys(successorsGroupedByPredecessors);
        const successorTasks = flatten(successorsGroupedByPredecessors);

        const [noParent] = predecessorTasks
        // Znajdź takie zadania, które nie ma następców
            .filter(pre => !successorTasks.includes(pre))
            // Posortuj, aby uzyskać pierwszego alfabetycznie
            .sort();
        delete successorsGroupedByPredecessors[noParent];

        for (const key in successorsGroupedByPredecessors) {
            if (!successorsGroupedByPredecessors.hasOwnProperty(key)) {
                continue;
            }
            successorsGroupedByPredecessors[key] = successorsGroupedByPredecessors[key].filter(el => el !== key);
        }

        queue = queue.concat(noParent);
    }

    return queue.join('') + lastTask;
}

export function createWorker(taskID, startTime) {
    return {
        task: taskID,
        removeTime: taskTime(taskID) + startTime,
        get endTime() {
            return this.removeTime - 1;
        },
    };
}


export function isTaskFinished(worker, now) {
    return worker.removeTime === now;
}

export function parrarelScheduler(inputData, workersLimit = WORKERS_LIMIT) {
    const successorsGroupedByPredecessors = buildDependenciesInfo(inputData);
    const lastTask = findLastTask(successorsGroupedByPredecessors);
    let workers = [];
    let currentSecond = 0;

    while (Object.values(successorsGroupedByPredecessors).length) {
        workers = workers.filter(worker => {
            const taskFinished = isTaskFinished(worker, currentSecond);
            if (taskFinished) {
                delete successorsGroupedByPredecessors[worker.task];
            }
            return !taskFinished;
        });

        const predecessorTasks = Object.keys(successorsGroupedByPredecessors);
        const successorTasks = flatten(successorsGroupedByPredecessors);

        workers = predecessorTasks
        // Znajdź takie zadania, które nie ma poprzedników i nie jest jeszcze wykonywane
            .filter(pre => !successorTasks.includes(pre) && !workers.filter(({task}) => task === pre).length)
            .sort()
            // Pobierz tylko, żeby wypełnić kolejkę pracownikami
            .slice(0, workersLimit - workers.length)
            .map(unary(partialRight(createWorker, currentSecond)))
            .concat(workers);

        if (workers.length) {
            currentSecond = Math.min(...workers.map(prop('removeTime')));
        } else {
            currentSecond++;
        }
    }

    return createWorker(lastTask, currentSecond).endTime;
}


export default function main([inputFilename = 'input.txt', workersLimit = WORKERS_LIMIT] = []) {
    const inputData = loadInputFile(inputFilename);
    console.log(parrarelScheduler(inputData, Number(workersLimit)));
}

