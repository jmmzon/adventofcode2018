import path from 'path';
import {createWorker, taskTime} from './index';
import {loadInputFile} from '../libs';

const inputFileAbs = (file) => path.resolve(path.join(__dirname, file));
const inputFile = loadInputFile(inputFileAbs('input.txt'));
const testFile = loadInputFile(inputFileAbs('test.txt'));

describe('day7: helper function tests', () => {
    it('should return correct task exec time', () => {
        expect(taskTime('A')).toBe(61);
        expect(taskTime('Z')).toBe(86);
    });

    it('should create task with correct finish time', () => {
        expect(createWorker('A', 15)).toEqual({
            task: 'A',
            endTime: 75,
            removeTime: 76,
        });
    });
});

describe('day7: real input file tests', () => {
    let parrarelScheduler, simpleScheduler;
    beforeEach(() => {
        parrarelScheduler = require('./index').parrarelScheduler;
        simpleScheduler = require('./index').simpleScheduler;
    });

    it('should part one returns correct order', () => {
        expect(simpleScheduler(inputFile)).toBe('GKPTSLUXBIJMNCADFOVHEWYQRZ');

    });

    it('should return correct total tasks execution time in main dataset', () => {
        expect(parrarelScheduler(inputFile)).toBe(920);
    });
});

describe('day7: test file tests', () => {
    let parrarelScheduler, simpleScheduler;
    beforeEach(() => {
        jest.resetModules();

        parrarelScheduler = require('./index').parrarelScheduler;
        simpleScheduler = require('./index').simpleScheduler;

        jest.mock('./constants', () => ({
            WORKERS_LIMIT: 2,
            BASE_EXECUTIOM_TIME: 0,
        }));
    });


    it('should part one returns correct order', () => {
        expect(simpleScheduler(testFile)).toBe('CABDFE');
    });


    it('should return correct total tasks execution time in test dataset', () => {
        expect(parrarelScheduler(testFile)).toBe(15);
    });
});








