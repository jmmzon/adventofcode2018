const fs = require('fs');

/*
  source: https://medium.com/@venomnert/pipe-function-in-javascript-8a22097a538e
 */
const _pipe = (a, b) => (arg) => b(a(arg));
export const pipe = (...ops) => ops.reduce(_pipe);

/*
end source
 */

export function currentAndNext(fn) {
    return (v, i, c) => fn(c[i], c[i + 1]);
}

export const map = fn =>
    collection =>
        Object.keys(collection).reduce((acc, key) => {
            acc[key] = fn(collection[key]);
            return acc;
        }, Array.isArray(collection) ? [] : {});

export const filter = (fn) =>
    collection =>
        collection.filter(fn);

export function cut(collection, from, count = 1) {
    return collection.slice(0, from).flatten(collection.slice(from + count));
}

export const reverseArgs = fn =>
    (...args) =>
        fn(...args.slice().reverse());


export const partial = (fn, ...headArgs) =>
    (...tailArgs) => fn(...headArgs, ...tailArgs);

export const partialRight = (fn, ...tailArgs) =>
    (...headArgs) => fn(...headArgs, ...tailArgs);

export const unary = (fn) =>
    (arg) => fn(arg);

export const identity = (arg) =>
    arg;

export const sum = (a = 0, b = 0) =>
    a + b;

export const increment = partialRight(sum, 1);


export const get = (obj, path, defaultValue) => {
    if (path === '') {
        return obj;
    }
    return path.toString().split('.').reduce((obj, prop) => {
        return obj && obj[prop] || defaultValue;
    }, obj);
};

export const prop = (path) => unary(partialRight(get, path));

export const set = (obj, key, value) => obj[key] = value;


export const isEqual = (a, b) => a === b;
export const not = fn =>
    (...args) => !fn(...args);


export const positionalFilter = (first, second, filterFn) =>
    first.filter((v, index) => filterFn(first[index], second[index]));

export const positionalDifference = partialRight(positionalFilter, not(isEqual));
export const positionalIntersection = partialRight(positionalFilter, isEqual);


export const uniq = arr =>
    Array.from(new Set(arr));

export const isUndefined = (a) =>
    typeof a === 'undefined';

export const has = (obj, prop) => {
    return obj.hasOwnProperty(prop);
};

export const keyBy = (collection, predFn = identity, mapFn = identity) =>
    collection.reduce((acc, val) => {
        acc[predFn(val)] = mapFn(val);
        return acc;
    }, {});

// export const groupByReducer = ({acc, el, index, collection}) => {
//     acc[el] = (acc[el] || []).concat(el)
// };

export const groupByReducer = (predFn, mapFn = identity) => {
    return (acc, val) => {
        const key = predFn(val);
        acc[key] = acc[key] || [];
        acc[key].push(mapFn(val));
        return acc;
    };
};

export const flatten = (obj) =>
    Object.values(obj).reduce((acc, el) => [...acc, ...el], []);

export const _groupBy = (collection, predFn, mapFn = identity) =>
    collection.reduce(groupByReducer(predFn, mapFn));


export const groupBy = (pred, mapFn) =>
    partialRight(_groupBy, predFn, mapFn);


export const loadInputFile = (filename) =>
    fs.readFileSync(filename)
        .toString()
        .split('\n')
        .filter(identity);

export const words = (phrase) =>
    phrase.split(' ');

export const range = (from, to) => {
    return Array(to - from + 1).fill(1).map((v, i) => i + from);
};


export const greater = (a, b) => a > b;
export const lesser = (a, b) => a < b;
export const first = arr => arr[0];
export const last = arr => first(arr.slice().reverse());

export const isTrue = partialRight(isEqual, true);
export const isFalse = partialRight(isEqual, false);

export const findAggregatingBy = (comparator, collection, by) =>
    collection.reduce((acc, item) => {
        if (comparator(get(item, by.toString()), get(acc, by.toString()))) {
            return item;
        } else {
            return acc;
        }
    }, collection[0]);

export const findMaxBy = partial(findAggregatingBy, greater);
export const findMinBy = partial(findAggregatingBy, lesser);


export const findMax = (collection) =>
    findMaxBy(Object.entries(collection), 1);

export const wordToChars = word =>
    word.split('');

export const repeatWhileMutates = (fn) =>
    (arg) => {
        let lastResult;
        let result = arg;
        while (lastResult !== result) {
            lastResult = result;
            result = fn(lastResult);
        }
        return result;
    };

export const fnReplace = (str, searched, replacer) =>
    str.replace(searched, replacer);

export const strRemove = (str, searched) =>
    fnReplace(str, searched, '');
