const {isUndefined} = require('.');
/**
 * @typedef {any} T
 */

/**
 * Returns handler for proxy, that if target property is undefined, returns given default value
 * @param {T} defaultValue
 * @returns {{get(*, *): (*|T)}}
 * @constructor
 */
const DefaultValueHandler = (defaultValue = 0) => ({
  get(target, name) {
    if(isUndefined(target[name])) {
      return defaultValue;
    }
    return target[name];
  },
});

module.exports = DefaultValueHandler;
