/**
 * Returns generator that iterates over array forever (and one day later)
 * @param {[]} arr
 * @returns {IterableIterator<[]>}
 * @constructor
 */
function* InfiniteIteratee(arr) {
  let i = 0;
  if(arr.length === 0) {
    return;
  }
  while(true) {
    yield arr[i];
    i = (i + 1) % arr.length;
  }
}

module.exports = InfiniteIteratee;
