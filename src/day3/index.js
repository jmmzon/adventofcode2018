const {loadInputFile, unary, greater, partialRight, get, pipe, filter, map, first, isFalse, not} = require('../libs');
const coordinateRegexp = /#(\d*) @ (\d*),(\d*): (\d*)x(\d*)/;

const getFabricsUsage = (coordinates) =>
  coordinates.reduce((acc, coordinates) => {
    const [, id, x, y, width, height] = coordinates.map(unary(parseInt));

    for (let i = x; i < x + width; i++) {
      for (let j = y; j < y + height; j++) {
        const coordsKey = `${i}x${j}`;
        if (!acc[coordsKey]) {
          acc[coordsKey] = {
            requests: 1,
            usedBy: [id],
          };
        } else {
          acc[coordsKey].requests++;
          acc[coordsKey].usedBy.push(id);
        }
      }
    }
    return acc;
  }, {});

const getRequestProperty = partialRight(get, 'requests');
const isGreaterThan1 = partialRight(greater, 1);

const getFabricsUsedByManyClaims = pipe(
  map(unary(getRequestProperty)),
  filter(unary(isGreaterThan1))
);

const getIntactClaim = fabricsUsageValues =>
  fabricsUsageValues.reduce((acc, {usedBy}) => {
    const isNotFalse = not(isFalse);
    if (usedBy.length === 1 && isNotFalse(acc[first(usedBy)])) {
      acc[usedBy[0]] = true;
    } else {
      usedBy.forEach(id => acc[id] = false);
    }
    return acc;
  }, {});

export default function main([filename = 'input.txt']) {
  const fileContent = loadInputFile(filename);
  const coordinates = fileContent.map(coordinateRegexp.exec.bind(coordinateRegexp));
  const fabricsUsage = getFabricsUsage(coordinates);
  const fabricsUsageValues = Object.values(fabricsUsage);
  const commonsUsedFabrics = getFabricsUsedByManyClaims(fabricsUsageValues);
  const getEntryValue = partialRight(get, 1);
  const intactClaim = pipe(
    getIntactClaim,
    Object.entries,
    filter(unary(getEntryValue))
  )(fabricsUsageValues);

  console.log('Part one result', commonsUsedFabrics.length);
  console.log('Part two result', intactClaim);
}
