class MarblesBoardItem {
    value;
    next;
    prev;

    constructor(value) {
        this.value = value;
    }
}

class MarblesBoard {
    head = null;
    current = null;

    moveCurrentPos = (offset) => {
        if (!this.head) {
            return;
        }
        if (offset > 0) {
            for (let i = 0; i < offset; i++) {
                this.current = this.current.next;
            }
        } else {
            for (let i = 0; i > offset; i--) {
                this.current = this.current.prev;
            }
        }
    };

    removeItem(pos) {
        this.moveCurrentPos(pos);
        if (this.current === this.head) {
            if (this.head.next === this.head) {
                this.head = null;
            } else {
                this.head = this.head.next;
            }
        }

        const item = this.current;
        item.prev.next = item.next;
        item.next.prev = item.prev;
        this.current = item.next;
        return item;
    }

    addItem(pos, value) {
        this.moveCurrentPos(pos);

        const item = new MarblesBoardItem(value);
        if (this.head) {
            item.next = this.current.next;
            item.prev = this.current;
            this.current.next.prev = item;
            this.current.next = item;
        } else {
            this.head = item;
            item.next = item;
            item.prev = item;
        }
        this.current = item;

        return item;
    }

    * [Symbol.iterator]() {
        let currentItem = this.head;

        do {
            yield currentItem.value;
            currentItem = currentItem.next;
        } while (currentItem !== this.head);
    }
}



export default function main() {
    const PLAYERS = 455;
    const MAX_MARBLE_WORTH = 7122300;
    const marblesBoard = new MarblesBoard();
    const elfsScores = [];

    for (let i = 0; i <= MAX_MARBLE_WORTH; i++) {
        const elfIndex = i % PLAYERS;
        if (i % 23 === 0 && i !== 0) {
            const {value} = marblesBoard.removeItem(-7);
            elfsScores[elfIndex] = (elfsScores[elfIndex] || 0) + i + value;
        } else {
            marblesBoard.addItem(1, i);
        }
    }

    console.log('Winning score is ', Math.max(...elfsScores));
}
