const {unary, sum, loadInputFile} = require('../libs');
const DefaultValueHandler = require('../libs/DefaultValueHandler');
const InfiniteIteratee = require('../libs/InfiniteIteratee');

const partTwo = (fileContent) => {
  const frequenciesOccurs = new Proxy({}, DefaultValueHandler(0));
  let sum = 0;

  for (const val of InfiniteIteratee(fileContent)) {
    sum += val;
    frequenciesOccurs[sum]++;
    if (1 < frequenciesOccurs[sum]) {
      return sum;
    }
  }
};


export default function main([filename = 'input.txt']) {
  const fileContent = loadInputFile(filename);

  const frequencyChangeSet = fileContent.map(unary(parseInt));
  const finalFrequency = frequencyChangeSet.reduce(sum, 0);

  console.log('Final frequency: ', finalFrequency);
  console.log('First frequency that appears twice: ', partTwo(frequencyChangeSet));
};


