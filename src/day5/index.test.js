import {uniqChars, reducePolymer, reducePolymersRemovingUnitTypes} from '.';

describe('day 5 test suite', () => {
    it('uniqChars test', () => {
        expect(uniqChars('Ala ma kota')).toEqual(['a', 'l', ' ', 'm', 'k', 'o', 't']);
    });

    it('reducePolymer test', () => {
        expect(reducePolymer('dabAcCaCBAcCcaDA')).toEqual('dabCBAcaDA');
    });

    it('reducePolymerRemobingUnitTypes test', () => {
        expect(reducePolymersRemovingUnitTypes('dabAcCaCBAcCcaDA'))
            .toEqual([
                {unitType: 'd', reducedPolymer: 'abCBAc', length: 6},
                {unitType: 'a', reducedPolymer: 'dbCBcD', length: 6},
                {unitType: 'b', reducedPolymer: 'daCAcaDA', length: 8},
                {unitType: 'c', reducedPolymer: 'daDA', length: 4},
            ]);
    });
});
