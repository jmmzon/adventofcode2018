import {
    loadInputFile,
    uniq,
    get,
    findMinBy,
    repeatWhileMutates, partialRight, strRemove,
} from '../libs';


export const shouldAnihilate = (a = '', b = '') =>
    a !== b && a.toUpperCase() === b.toUpperCase();

export const reducePolymer = repeatWhileMutates(function _reducePolymer(polymer) {
    const newPolymer = [];
    let i = 0;
    while (i < polymer.length) {
        if (shouldAnihilate(polymer[i], polymer[i + 1])) {
            i += 2;
        } else {
            newPolymer.push(polymer[i]);
            i += 1;
        }
    }
    return newPolymer.join('');
});

export const uniqChars = input =>
    uniq(input.toLowerCase().split(''));

export const reducePolymersRemovingUnitTypes = polymer =>
    uniqChars(polymer).reduce((summaries, unitType) => {
        const removeUnitType = partialRight(strRemove, new RegExp(unitType, 'gi'));
        const summary = {
            unitType,
            reducedPolymer: reducePolymer(removeUnitType(polymer)),
        };
        summary.length = summary.reducedPolymer.length;
        return summaries.concat(summary);
    }, []);


export default function main([filename = 'input.txt']) {
    const [polymer] = loadInputFile(filename);
    const summaries = reducePolymersRemovingUnitTypes(polymer);

    console.log('Part one result', get(reducePolymer(polymer), 'length'));
    console.log('Part two result', get(findMinBy(summaries, 'length'), 'length'));
};
