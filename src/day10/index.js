import {first, loadInputFile, pipe} from "../libs";
import fs from 'fs';
const inputRegex = /[-]{0,1}[\d]*[\.]{0,1}[\d]+/g;


function parseInput(input) {
    return input.map(v => [...v.matchAll(inputRegex)].map(pipe(first, Number)));
}

function makeArray(size, defaultValueFactory) {
    let arr = [];

    for(let i = 0; i < size; i++) {
        arr[i] = defaultValueFactory();
    }
    return arr.map(defaultValueFactory);
}

function printSky(coordinates) {
    const maxX = Math.max(...coordinates.map(a => a[0])) + 20;
    const maxY = Math.max(...coordinates.map(a => a[1])) + 20;
    const board = makeArray(maxY, () => makeArray(maxX, () => '.'));


    coordinates.forEach(([y, x]) => {
        board[x][y] = 'X';
    });


    return board.map(r => r.join('')).join('\r\n');
}


export default function main([inputFilename = 'input.txt']) {
    let inputData = parseInput(loadInputFile(inputFilename));
    let maxxxX = Infinity;
    let i = 0;

    for (; i < 1000000; i++) {
        inputData = inputData.map(([x, y, vx, vy]) => {
            return [x + vx, y + vy, vx, vy];
        });
        // const maxX = Math.max(...inputData.map(a => a[0]));
        // const minX = Math.min(...inputData.map(a => a[0]));
        // const maxY = Math.max(...inputData.map(a => a[1]));
        // const minY = Math.min(...inputData.map(a => a[1]));
        if (10230 < i && i < 10252) {
            fs.writeFileSync('output', i + '='.repeat(250) + '\r\n\r\n', {flag: 'a'});
            fs.writeFileSync('output', printSky(inputData), {flag: 'a'});
        } else if (i > 10252) {
            return;
        }
    }
}
