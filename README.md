This repo contain code with my implementation of ["Advent of code 2018"](https://adventofcode.com/2018) event.
Every day has own input file. Almost whole code is written by me, exceptions for this rule are marked

**Install**

`yarn install`

**Run**

`yarn start _day_`

where day is day name

For example: 
`yarn start day1`

**Tests**

`yarn test`

**Build**

`yarn build`