import {findDifferenceAtOne, countCharacters} from '../../src/day2/index';

describe('Day 2 test', () => {
    it('findDifferenceAtOne test', () => {
        const strings = [
            'Ala ma kota',
            'Ala ma buta',
            'Ala ma dom',
            'Ola ma buta',
        ];

        expect(findDifferenceAtOne(strings)).toEqual([
            'Ala ma buta',
            'Ola ma buta',
        ]);
    });

    it('findDifferenceAtOne test, when no strings should be found', () => {
        const strings = [
            'Ala ma kota',
            'Ala ma buta',
            'Ala ma dom',
            'Ewa ma buta',
        ];

        expect(findDifferenceAtOne(strings)).toEqual([]);
    });

    it('countCharacters test', () => {
        expect(countCharacters('Ala ma kotka')).toEqual({
            'A': 1,
            'l': 1,
            'a': 3,
            'm': 1,
            ' ': 2,
            'o': 1,
            'k': 2,
            't': 1,
        });
    });
});
