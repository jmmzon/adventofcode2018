const InfiniteIteratee = require('../../src/libs/InfiniteIteratee');


it('InifinityArray', () => {
  const arr = [1, 2];
  const infiniteArrayIterator = InfiniteIteratee(arr);

  expect(infiniteArrayIterator.next()).toEqual({value: 1, done: false});
  expect(infiniteArrayIterator.next()).toEqual({value: 2, done: false});
  expect(infiniteArrayIterator.next()).toEqual({value: 1, done: false});
  expect(infiniteArrayIterator.next()).toEqual({value: 2, done: false});
  for(let i = 0; i < 20; i++) {
    infiniteArrayIterator.next();
  }
  expect(infiniteArrayIterator.next().done).toBe(false);
});

it('empty InfiniteArray', () => {
  const infiniteArrayIterator = InfiniteIteratee([]);
  expect(infiniteArrayIterator.next().done).toBe(true);
});
