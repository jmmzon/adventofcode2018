const fs = require('fs');
jest.mock('fs');
const {
    unary,
    identity,
    isUndefined,
    sum,
    increment,
    partial,
    partialRight,
    isEqual,
    not,
    keyBy,
    get,
    loadInputFile,
    positionalDifference,
    positionalIntersection,
    has,
    uniq,
    findMaxBy,
    flatten,
    map,
    filter,
    repeatWhileMutates,
} = require('../../src/libs/index');


describe('libs test suite', () => {
    it('unary factory test', () => {
        const testFn = jest.fn();
        const testFnUnary = unary(testFn);

        testFnUnary(1, 2, 3, 4);

        expect(testFn).toBeCalledWith(1);
    });

    it('identity test', () => {
        const input = {a: 12};
        const output = identity(input);

        expect(input).toBe(output);
    });

    it('test isUndefined', () => {
        expect(isUndefined(null)).toBe(false);
        expect(isUndefined({})).toBe(false);
        expect(isUndefined('')).toBe(false);
        expect(isUndefined(12)).toBe(false);
        expect(isUndefined(undefined)).toBe(true);
    });

    it('sum test', () => {
        expect(sum(21, 37)).toBe(58);
    });


    it('partial test', () => {
        const fn = jest.fn();
        const partialFn = partial(fn, 1, 2, 3);
        partialFn(4, 5, 6);

        expect(fn).toBeCalledWith(1, 2, 3, 4, 5, 6);
    });

    it('partialRight test', () => {
        const fn = jest.fn();
        const partialRightFn = partialRight(fn, 1, 2, 3);
        partialRightFn(4, 5, 6);

        expect(fn).toBeCalledWith(4, 5, 6, 1, 2, 3);
    });

    it('isEqual test', () => {
        expect(isEqual(1, 2)).toBe(false);
        expect(isEqual(2, 2)).toBe(true);
    });

    it('not function test', () => {
        const fn = () => true;
        const notFn = not(fn);
        expect(notFn()).toBe(false);
    });

    it('get function test', () => {
        const inputObj = {
            firstName: 'Jan',
            address: {
                city: 'Kraków',
                id: 22,
            },
        };

        expect(get(inputObj, 'firstName')).toBe('Jan');
        expect(get(inputObj, 'lastName', 'unknown')).toBe('unknown');
        expect(get(inputObj, 'address.city')).toBe('Kraków');
        expect(get(inputObj, 'address.country.id', 404)).toBe(404);
    });

    it('keyBy test with no fn passed', () => {
        const input = [1, 2, 3];
        const output = keyBy(input);
        expect(output).toEqual({
            1: 1,
            2: 2,
            3: 3,
        });
    });

    it('keyBy test, with fn passed', () => {
        const input = [{id: 1, name: 'Piotr'}, {id: 2, name: 'Jan'}];
        const expectedOutput = {
            1: {
                id: 1,
                name: 'Piotr',
            },
            2: {
                id: 2,
                name: 'Jan',
            },
        };

        expect(keyBy(input, partialRight(get, 'id'))).toEqual(expectedOutput);
    });

    it('test loadInput file', () => {
        fs.readFileSync.mockReturnValue('Jan\nPiotrs\nMikolaj\n');
        const output = loadInputFile('dane.txt');

        expect(fs.readFileSync).toBeCalledWith('dane.txt');
        expect(output).toEqual(['Jan', 'Piotrs', 'Mikolaj']);
    });

    it('test positinalDifference', () => {
        const arr1 = [1, 2, 3, 4, 5];
        const arr2 = [3, 2, 1, 4, 0];

        expect(positionalDifference(arr1, arr2)).toEqual([1, 3, 5]);
    });

    it('test positionalIntersection', () => {
        const arr1 = [1, 2, 3, 4, 5];
        const arr2 = [3, 2, 1, 4, 0];

        expect(positionalIntersection(arr1, arr2)).toEqual([2, 4]);
    });

    it('has function test', () => {
        const parent = {firstName: 'Jan'};
        const child = Object.create(parent);
        expect(has(parent, 'firstName')).toBe(true);
        expect(has(child, 'firstName')).toBe(false);
    });

    it('uniq funciton test', () => {
        expect(uniq([1, 2, 10, 1, 5, 2])).toEqual([1, 2, 10, 5]);
    });

    it('increment test', () => {
        expect(increment(5)).toBe(6);
    });

    it('findMaxBy test', () => {
        const input = [
            {id: 1, name: 'Piotr', age: 14},
            {id: 2, name: 'Adam', age: 65},
            {id: 3, name: 'Mikołaj', age: 4},
        ];

        const expectedOutput = input[1];
        expect(findMaxBy(input, 'age')).toEqual(expectedOutput);
    });


    it('test filter funciton', () => {
        const input = [1, 2, 3, 4, 5];
        const filterFn = filter((a) => a > 2);

        expect(filterFn(input)).toEqual([3, 4, 5]);
    });

    it('test map funciton', () => {
        const input = [1, 2, 3];
        const double = map((a) => a * 2);

        expect(double(input)).toEqual([2, 4, 6]);
    });

    it('repeatWhileMutates test', () => {
        const jestFn = jest.fn();
        const incrementMaxTo4 = (arg) => {
            jestFn(arg);
            return Math.min(arg + 1, 4);
        };
        expect(incrementMaxTo4(2)).toBe(3);
        expect(incrementMaxTo4(5)).toBe(4);

        const incrementUpTo4 = repeatWhileMutates(incrementMaxTo4);

        incrementUpTo4(0);
        expect(jestFn).toBeCalledTimes(7);

        /* Thanks to: https://stackoverflow.com/questions/40018216/how-to-check-multiple-arguments-on-multiple-calls-for-jest-spies?answertab=votes#tab-top*/
        expect(jestFn.mock.calls).toEqual([
            [2],
            [5],
            [0],
            [1],
            [2],
            [3],
            [4],
        ]);

    });

    it('should flatten array of arrays', () => {
        expect(flatten([[1, 2], [2, 3]])).toEqual([1, 2, 2, 3]);
    });

    it('should flatten dictionary of arrays', () => {
        expect(flatten({
            1: [1, 2, 3],
            id: [5, 6],
        })).toEqual([1, 2, 3, 5, 6]);
    });
});
