const DefaultValueHandler = require('../../src/libs/DefaultValueHandler');

it('DefaultValueHandler', () => {
  const defaultValue = 'Nie ma nic';
  const handler = DefaultValueHandler(defaultValue);

  expect(typeof handler).toBe('object');
  expect(typeof handler.get).toBe('function');

  const proxiedObj = new Proxy({firstName: 'Jan', id: null}, handler);

  expect(proxiedObj.firstName).toBe('Jan');
  expect(proxiedObj.id).toBe(null);
  expect(proxiedObj.lastName).toBe('Nie ma nic');
  proxiedObj.lastName = 'Zoń';
  expect(proxiedObj.lastName).toBe('Zoń');

});
