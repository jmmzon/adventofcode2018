import {distance, closestIndex, getDistances} from '../../src/day6';

const nodes = [
    [1, 1],
    [1, 6],
    [8, 3],
    [3, 4],
    [5, 5],
    [8, 9],
];

describe('day 6 test', () => {
    it('should calculate manhattan distance', () => {
        expect(
            distance(
                [1, 23],
                [3, 12]
            )
        ).toBe(13);
    });

    it('should return getDistances', () => {
        expect(getDistances([
                nodes[2],
                nodes[3],
            ],
            [1, 2]
        )).toEqual([
            8,
            4,
        ]);
    });

    it('should find nearest element', () => {
        expect(closestIndex([1, 3, 2, 10])).toEqual(0);
        expect(closestIndex([2, 3, 2, 10])).toEqual(-1);
    });


});
